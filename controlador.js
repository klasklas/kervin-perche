

$('#id_Ges_Trabajadores').on('click',function(e){
    e.preventDefault();
   QuitarPaneles();
    $('#id-frm_Gest_trabajadores').removeClass('d-none');
    $('#1').removeClass('d-none');
    $('#2').removeClass('d-none');
    cargarTrabajadores();

});

$('#id_Agr_Trabajadores').on('click',function(e){
    e.preventDefault();
    QuitarPaneles();
    $('#id_new_trabajador_alert').addClass('d-none');
    $('#id-frm_newTrabajador').removeClass('d-none');
    $('#1').removeClass('d-none');
    $('#2').removeClass('d-none');

});

$('#id_Ges_Reportes').on('click',function(e){
    e.preventDefault();
    QuitarPaneles();
    $('#id-frm_Gest_reportes').removeClass('d-none');
    $('#1').removeClass('d-none');
    $('#2').removeClass('d-none');

});


$('#id_Tabla_Trabajadores').on('click',function(e){
    e.preventDefault();
   // console.log(e.target.offsetParent.parentElement.children[1].innerHTML);
    //console.log(e.target.attributes[0].value);
    try {
        
        let accion = e.target.attributes[0].value;
          

        if(accion == 'borrar' || accion=='editar'){
            let Usu_aModificar = e.target.offsetParent.parentElement.children[1].innerHTML;

            if(accion == 'borrar'){
                // console.log('se borrara '+Usu_aModificar);
                let confirmacion=  confirm ('Esta seguro de ELIMINAR el registro de '+Usu_aModificar+'?');
                
                if(confirmacion){
                    console.log(confirmacion);
                    eliminarTrabajador(Usu_aModificar);
                }

            }else{

               editarTrabajador(Usu_aModificar);
            }
        }
    } catch (error) {
        
    }
});


$('#id_btn_edit').on('click', (function(e){

    e.preventDefault();

    $.ajax({
        type:"post",
        url: "ModificarTrabajador.php",
       
        dataType: 'json',
        success: function (texto){
           
            QuitarPaneles();
             $('#id-frm_Edit_Trabajador').removeClass('d-none');
            $('#id_edit_nombre').val(texto[0].nombre);
            $('#id_edit_email').val(texto[0].correo);
            $('#id_edit_fono').val(texto[0].telefono);
            $('#id_edit_fecha').val(texto[0].fechadeingreso);
            $('#id_edit_clave').val(texto[0].clave);
           
        }
    });


}));


$('#id_Fin_seccion').on('click', function(e){
    e.preventDefault();
    datos=null;
    QuitarPaneles();
    $('#id_ventanaPrincipal').removeClass('d-none')
});


$('#id_edit_Trabajador').on('click',(function(e){
    e.preventDefault();
    // QuitarPaneles();
    
    try{

        var datos2 = $('#id_edit_Trabajador').serialize();
         
        if(e.target.name == 'nm_edit_trabajador'){

            $.ajax({
                type:"post",
                url: "guardarTrabajador.php",
                data: datos2,
                success: function (texto){

                    console.log(texto);

                    if(texto=='exito'){

                        $('#id_actualizacion_trabajador').removeClass('d-none');
                        $('#id_actualizacion_trabajador').removeClass('alert-danger');
                        $('#id_actualizacion_trabajador').addClass(' alert-success');
                        $('#id_actualizacion_trabajador').html('Datos Correctamente Actualizados');
                        $('#id_edit_Trabajador')[0].reset();

                    }else{
                        $('#id_actualizacion_trabajador').removeClass('d-none');
                        $('#id_actualizacion_trabajador').addClass('alert alert-danger');
                        $('#id_actualizacion_trabajador').html('Datos No Actualizados. Error<br>'+texto);

                    }
                }
            });
        }
    }catch(error){

    }
   
}));


$('#id_new_Trabajador').on('click', function(e){
    e.preventDefault();
    try{
        var datosqt = $('#id_new_Trabajador').serialize();

            if(e.target.name == 'nm_guardar_trabajador'){
               // console.log(e.target.name);
                $.ajax({
                    type:"post",
                    url: "guardarTrabajador.php",
                    data: datosqt,
                    success: function (texto){
                         console.log(texto);
                        if(texto=='exito'){
                            Guardado();
                            //console.log('Trabajador Guardado Exitosamente');
                        }else{
                             Error_Acceso2(texto);
                           //  console.log('No Guardado, error al accesar a la base de datos');
                        }
                    }
                });
            }
    }catch(error){

    }
});



$('#formulario').submit(function(e){
    e.preventDefault();

    procesar( );
});

function procesar(datos, editar){
    var datos = $('#formulario').serialize();
    //console.log(datos);

    $.ajax({
        type:"post",
        url: "contenidos.php",
        data: datos,
        success: function (texto){
            if(texto =="acceso"){
                QuitarPaneles();
                $('#2').removeClass('d-none');
                $('#1').removeClass('d-none');
            }else{
                $('#id_alerta1').removeClass('d-none');
                $('#id_alerta1').html(texto);
            }
            
        }
    });
}


function editarTrabajador(mail){

    var envio= {accion:'editarTrabajador', correo: mail};

    $.ajax({
        type:"post",
        url: "ModificarTrabajador.php",
        data: envio,
        dataType: 'json',
        success: function (texto){
           
            QuitarPaneles();
            $('#id-frm_Edit_Trabajador').removeClass('d-none');
            $('#1').removeClass('d-none');
            $('#2').removeClass('d-none');
            $('#id_edit_nombre').val(texto[0].nombre);
            $('#id_edit_email').val(texto[0].correo);
            $('#id_edit_fono').val(texto[0].telefono);
            $('#id_edit_fecha').val(texto[0].fechadeingreso);
            $('#id_edit_clave').val(texto[0].clave);
           
        }
    });
}

function eliminarTrabajador(mail){

    var envio= {accion:'eliminarTrabajador', correo: mail};

    $.ajax({
        type:"post",
        url: "ModificarTrabajador.php",
        data: envio,
        dataType: 'json',
        success: function (texto){
           
            if(texto){
                cargarTrabajadores();
            }else{
                alert("Ocurrio un error al eliminar de la base de datos");
            }
        }
    });

}



function Error_Acceso2(texto){
    $('#id_new_trabajador_alert').addClass('alert alert-danger');
    $('#id_new_trabajador_alert').removeClass('d-none');

    if(texto=='error'){
        $('#id_new_trabajador_alert').html('No Guardado, error al accesar a la base de datos'); 
    }else{
    $('#id_new_trabajador_alert').html(texto);
    }
}

function Guardado(){
    $('#id_new_trabajador_alert').removeClass('alert alert-danger');
    $('#id_new_trabajador_alert').addClass('alert alert-success');
    $('#id_new_trabajador_alert').removeClass('d-none');

    $('#id_new_trabajador_alert').html('Trabajador Guardado Exitosamente');
    
    $('#id_new_Trabajador')[0].reset();

}


function QuitarPaneles(){
    $('#id_ventanaPrincipal').addClass('d-none');
    $('#id-frm_Gest_trabajadores').addClass('d-none');
    $('#id-frm_newTrabajador').addClass('d-none');
    $('#id-frm_Gest_reportes').addClass('d-none');
    $('#id-frm_Edit_Trabajador').addClass('d-none');
    $('#id_ventanaPrincipal').addClass('d-none');
    $('#id_actualizacion_trabajador').addClass('d-none');
    $('#1').addClass('d-none');
    $('#2').addClass('d-none');
    

}

function cargarTrabajadores(){

    $.ajax({
        type:"post",
        url: "ModificarTrabajador.php",
        data: {accion:'obtenerTrabajadores',correo:''},
        dataType: 'json',
        success: function (resp){
          
            var trab='';
            resp.forEach(element => {
            trab+=`
                <tr>
                    <th scope="row">${element.nombre}</th>
                    <td>${element.correo}</td>
                    <td>${element.telefono}</td>
                    <td>${element.fechadeingreso}</td>
                    <td>
                        <a href="" id="id_Edita_trabajador">
                            <i name="editar" style="color:#0080FF" class="fas fa-user-edit"></i>
                        </a> 
                        
                        <a href="" title="hola" style='a[tittle]{color:red}'>
                            <i name="borrar" class="far fa-trash-alt " style="color: #FE2E2E"></i> 
                        </a>
                    </td>
                </tr>`
            });

            $('#id_tbody_Trabajadores').html(trab);
            
        }
    });

}


