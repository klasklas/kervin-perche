<?php

include_once 'conexion.php';

$error ='';
$clave='';
$email='';

if(empty($_POST['nm_email'])){
    $error .= 'Por favor Ingrese el Email de usuario<br>';
}else{
    $email = $_POST['nm_email'];
    if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
        $error .= '<br>Por favor Ingrese el Email valido!<br>';
    }else{
        $email = filter_var($email, FILTER_SANITIZE_STRING);
        $email = trim($email);
    }
}

if(empty($_POST['nm_clave'])){
    $error .= 'Por favor Ingrese Clave<br>';
}else{
    $clave = $_POST['nm_clave'];
    $clave = filter_var($clave, FILTER_SANITIZE_STRING);
    $clave = trim($clave);
}

if ($error == ''){
    echo 'acceso';
}else{
    echo $error;
    // $error='';
}

?>