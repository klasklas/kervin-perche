<?php

 

$error ='';
$nombre='';
$email='';
$fono='';
$fecha='';
$clave='';
$editar='';

$editar = $_POST['editar'];


if(empty($_POST['nm_nombre'])){
    $error .= 'Por favor Ingrese Nombre<br>';
}else{
    $nombre = $_POST['nm_nombre'];
    $nombre = filter_var($nombre, FILTER_SANITIZE_STRING);
    $nombre = trim($nombre);
}


if(empty($_POST['nm_email'])){
    $error .= 'Por favor Ingrese el Email de usuario<br>';
}else{
    $email = $_POST['nm_email'];
    if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
        $error .= '<br>Por favor Ingrese el Email valido!<br>';
    }else{
        $email = filter_var($email, FILTER_SANITIZE_STRING);
        $email = trim($email);
    }
}

if(empty($_POST['nm_fecha'])){
    $error .= 'Por favor Ingrese fecha<br>';
}else{
    $fecha = $_POST['nm_fecha'];
    $fecha = filter_var($fecha, FILTER_SANITIZE_STRING);
    $fecha = trim($fecha);
}

if(empty($_POST['nm_fono'])){
    $error .= 'Por favor Ingrese telefono<br>';
}else{
    $fono = $_POST['nm_fono'];
    $fono = filter_var($fono, FILTER_SANITIZE_STRING);
    $fono = trim($fono);
}

if(empty($_POST['nm_clave'])){
    $error .= 'Por favor Ingrese Clave<br>';
}else{
    $clave = $_POST['nm_clave'];
    $clave = filter_var($clave, FILTER_SANITIZE_STRING);
    $clave = trim($clave);
}


if ($error == '' ){
    if($editar=='false'){ Guardar_en_DB(); }
    if($editar=='true'){ Actualizar_en_DB(); }
    
    
}else{
    echo $error;
    // $error='';
}

function Guardar_en_DB(){

    global $nombre; global $email; global $fono; global $fecha; global $clave; global $editar;

    include_once 'conexion.php';
   // echo $nombre.' '.$email.' '.$fono.' '.$fecha.' '.$clave.' '.$email;


    try {
        $sqlX = "INSERT INTO trabajadores (nombre,correo,telefono,fechadeingreso,clave) VALUES (?,?,?,?,?)";
        $sentenciaT = $pdo->prepare($sqlX);
      $valor=  $sentenciaT->execute(array($nombre,$email,$fono,$fecha,$clave));

        if($valor){
            echo 'exito';
        }else{
            echo 'error';
        }

    } catch (PDOException $e) {
       print "¡Error!: " . $e->getMessage() . "<br/>";
        die();
    }
}

function  Actualizar_en_DB(){

    
     global $nombre; global $email; global $fono; global $fecha; global $clave; global $editar;
    // echo $nombre.' '.$email.' '.$fono.' '.$fecha.' '.$clave.' '.$email;

    include_once 'conexion.php';

    try {
        // $sqlX = "UPDATE trabajadores SET (nombre,correo,telefono,fechadeingreso,clave) VALUES (?,?,?,?) WHERE correo=?";
        $sqll = "UPDATE trabajadores SET nombre=?,correo=?,telefono=?,fechadeingreso=?,clave=? WHERE correo=?";

        $sentenciaTa = $pdo->prepare($sqll);
      $valorz=  $sentenciaTa->execute(array($nombre,$email,$fono,$fecha,$clave,$email));

        if($valorz){
            echo 'exito';
        }else{
            echo 'error';
        }

    } catch (PDOException $e) {
       // print "¡Error!: " . $e->getMessage() . "<br/>";
        die();
    }
}





?>